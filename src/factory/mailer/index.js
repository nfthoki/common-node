import _ from 'lodash'
import * as smtp from './smtp'
import {send} from './send'

const providers = {
  smtp,
}

const dbIdentify = (ctx, transporter) => {
  return {
    transporter,
    send: (body) => send(ctx, transporter, body),
  }
}

const init = async (ctx, config) => {
  const provider = _.get(providers, config.provider)
  if (!provider) {
    throw new Error('[mailer] provider is not supported')
  }

  const transporter = await provider.init(ctx, config.transport)

  return {mailer: dbIdentify(ctx, transporter)}
}

export default {init}
