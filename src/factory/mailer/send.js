const send = (ctx, transporter, body) =>
  new Promise((resolve, reject) => {
    transporter.sendMail(body, (err, result) =>
      err ? reject(err) : resolve(result)
    )
  })

export {send}
