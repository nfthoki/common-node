import _ from 'lodash'
import Bluebird from 'bluebird'
import Redis from 'ioredis'

import * as Verb from './redis.verb'

export const init = async (ctx, options) => {
  const {
    instances: {logger},
  } = ctx
  const redis = await new Bluebird((resolve) => {
    const redis = new Redis(options)

    redis.on('connect', () => resolve(redis))
    redis.on('error', (err) => {
      logger.error('Redis error', err)
    })

    redis.on('close', () => {
      logger.warn('Redis closed')
    })
  })

  _.assign(redis, Verb.bind(redis))

  return {redis}
}

export const toKey = (...paths) => _.join(_.flattenDeep(paths), ':')
