import {init, fromMongodb} from './agenda'

export default {init, fromMongodb}
