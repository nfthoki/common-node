import * as Vault from './vault'
import {assignObjOnce} from '/libs/object'

const utilize = (ctx, config, client) => {
  const vault = {
    client,
    lastLoginAt: -1,
    expiredAt: 0,
  }

  assignObjOnce(vault, {
    read: Vault.read(ctx, config, vault),
    write: Vault.write(ctx, config, vault),
    remove: Vault.remove(ctx, config, vault),
  })

  return vault
}

const init = async (ctx, config) => {
  const {vault} = await Vault.init(ctx, config)

  return {vault: utilize(ctx, config, vault)}
}

export default {init}
