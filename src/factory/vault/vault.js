import _ from 'lodash'
import moment from 'moment'
import Path from 'path'
import Vault from 'node-vault'

const init = async (ctx, config) => {
  const {endpoint} = config
  const vault = await Vault({
    endpoint,
  })

  return {vault}
}

const login = async (ctx, config, vault) => {
  const {
    instances: {logger},
  } = ctx
  const {roleId, secretId} = config
  const {client} = vault

  const loginResult = await client.approleLogin({
    role_id: roleId,
    secret_id: secretId,
  })
  const token = _.get(loginResult, 'auth.client_token')
  const leaseDuration = _.get(loginResult, 'auth.lease_duration')
  if (_.isEmpty(token)) {
    logger.error('[vault][login] Could not get client_token', loginResult)
    throw new Error('[vault][login] Could not get client_token')
  }

  return {token, expires: leaseDuration}
}

const autoRenew = async (ctx, config, vault) => {
  const {client} = vault

  if (_.isEmpty(client.token) || moment().isAfter(vault.expiredAt)) {
    const requestedAt = moment()
    const {expires} = await login(ctx, config, vault)

    vault.lastLoginAt = requestedAt.valueOf()
    vault.expiredAt = requestedAt.add(expires, 'seconds').valueOf()
  }

  return vault
}

const read = (ctx, config, vault) => {
  const {root} = config
  const {client} = vault

  return async (...paths) => {
    await autoRenew(ctx, config, vault)

    const result = await client.read(
      `${root}/data/${Path.join(..._.map(paths, _.toString))}`
    )

    return result
  }
}

const write = (ctx, config, vault) => {
  const {root} = config
  const {client} = vault

  return async (value, ...paths) => {
    await autoRenew(ctx, config, vault)

    const result = await client.write(
      `${root}/data/${Path.join(..._.map(paths, _.toString))}`,
      value
    )

    return result
  }
}

const remove = (ctx, config, vault) => {
  const {root} = config
  const {client} = vault

  return async (...paths) => {
    await autoRenew(ctx, config, vault)

    const result = await client.delete(
      `${root}/metadata/${Path.join(..._.map(paths, _.toString))}`
    )

    return result
  }
}

export {init, read, write, remove}
