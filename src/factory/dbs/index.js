import _ from 'lodash'
import Path from 'path'
import Glob from 'glob'

import Mongodb from './mongodb'

const loadDaos = (ctx, dbs) => {
  const {
    instances: {logger},
  } = ctx

  const daoFiles = Glob.sync(Path.join(global.MAIN_DIR, 'daos/*.js'), {
    dot: true,
  })

  return _.fromPairs(
    _.map(daoFiles, (filePath) => {
      const daoFns = require(filePath)
      const {name: filename} = Path.parse(filePath)
      const daoName = _.upperFirst(_.camelCase(filename))

      logger.info(`-> dao: ${daoName}`)

      return [
        daoName,
        _.mapValues(daoFns, (fn) =>
          _.partial(fn, {...ctx, db: dbs.primary, dbs})
        ),
      ]
    })
  )
}

const init = async (ctx, dbConfig, listeners) => {
  const dbModels = require(Path.join(global.MAIN_DIR, 'models')).default
  const mongodb = await Mongodb.init(ctx, dbConfig, dbModels, listeners)

  const dbs = {
    ...mongodb.mongoDbByModels,
  }

  return {
    $: mongodb,
    db: dbs.primary,
    dbs,
    daos: loadDaos(ctx, dbs),
  }
}

export default {init}
