import path from 'path'
import {assignObjOnce} from '/libs/object'
import {parse as parseConfig} from '/libs/config'
import Databases from '/factory/dbs'
import Logger from '/factory/logger'

import {seed as mongodbSeed} from '../mongodb/seed'
import {confirmation} from '../confirmation'

const exec = async () => {
  const rawConfig = require(path.join(global.MAIN_DIR, 'config')).default
  const config = await parseConfig(rawConfig)

  const {logger} = await Logger.init({config})

  const ctx = assignObjOnce(
    {},
    {
      config,
      instances: {logger},
    }
  )

  const dbConfig = ctx.config.databases
  const databases = await Databases.init(ctx, dbConfig)
  assignObjOnce(ctx.instances, databases)

  const targetDb = process.env.SCRIPT_SEED_DB_NAME || 'primary'
  if (!dbConfig[targetDb]) {
    throw new Error(`No config for [${targetDb}]`)
  }

  await confirmation(
    ctx,
    {
      name: 'SEED',
      warning: 'Press Control + C for cancelling',
      params: {targetDb},
    },
    3
  )

  if (dbConfig[targetDb].type === 'mongodb') {
    await mongodbSeed(ctx, databases.dbs, targetDb)
  }
}

export default exec().catch(console.error)
