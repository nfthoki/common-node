import Bluebird from 'bluebird'

export const confirmation = async (ctx, info, seconds = 5) => {
  const {
    instances: {logger},
  } = ctx
  let counter = 0

  const waitingInSeconds = process.env.IGNORE_SCRIPT_CONFIRMATION ? 0 : seconds

  logger.warn(info.name)
  logger.warn(info.warning)
  logger.warn(info.params)

  return new Bluebird((resolve) => {
    const confirmInterval = setInterval(() => {
      counter++
      if (counter < waitingInSeconds) {
        logger.warn(
          `Performing ${info.name} in ${waitingInSeconds - counter} seconds...`
        )
        return
      }

      clearInterval(confirmInterval)
      logger.warn(`Performing ${info.name}...`)
      resolve()
    }, 1000)
  })
}
