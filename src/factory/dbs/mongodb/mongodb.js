import _ from 'lodash'
import moment from 'moment'
import Bluebird from 'bluebird'
import {MongoClient, ObjectId} from 'mongodb'
import {parse as parseConfig} from '/libs/config'
import * as Verb from './verb'

let _eventHandlers = {
  onConnected: _.stubTrue,
  onConnectError: _.stubTrue,
  onClosed: _.stubTrue,
  onPingFailed: _.stubTrue,
  onPingSucceed: _.stubTrue,
  onPingError: _.stubTrue,
}
const listeners = (logger) => ({
  onConnected: (dbName) => {
    logger.info(`Mongodb [${dbName}] connected...`)
    _eventHandlers.onConnected(dbName)
  },
  onConnectError: (dbName, err) => {
    logger.error(`Mongodb [${dbName}] error...`, err)
    _eventHandlers.onConnectError(dbName, err)
  },
  onClosed: (dbName) => {
    logger.warn(`Mongodb [${dbName}] closed...`)
    _eventHandlers.onClosed(dbName)
  },
  onPingFailed: (dbName) => {
    logger.debug(`Mongodb [${dbName}] ping failed`)
    _eventHandlers.onPingFailed(dbName)
  },
  onPingSucceed: (dbName) => {
    logger.debug(`Mongodb [${dbName}] ping success`)
    _eventHandlers.onPingSucceed(dbName)
  },
  onPingError: (dbName) => {
    logger.warn(`Mongodb [${dbName}] ping reaching threshold`)
    _eventHandlers.onPingError(dbName)
  },
})

const selectCollection = (db, name, prefix) => {
  return db.collection(`${prefix || ''}${name}`)
}

const getModel = (db, dbModel, dbPrefix) => {
  return function getModel(name, data) {
    const model = dbModel[name]

    if (!model) {
      throw new Error(`missing model [${name}]`)
    }

    const type = _.get(model, ['suffix', 'type'])
    const format = _.get(model, ['suffix', 'format'], '')

    let collection
    if (type === 'moment') {
      const formatted = format ? moment.utc(data).format(format) : ''

      collection = selectCollection(db, `${model.name}${formatted}`, dbPrefix)
    } else if (type === 'template') {
      const formatted = _.template(format)(data)

      collection = selectCollection(db, `${model.name}${formatted}`, dbPrefix)
    } else {
      collection = selectCollection(db, `${model.name}${format}`, dbPrefix)
    }

    return Verb.bind(collection, model)
  }
}

const useCollection = (conn, dbModel, dbPrefix) => {
  const modelByName = getModel(conn, dbModel, dbPrefix)

  return function useCollection(collection) {
    const model = _.findKey(dbModel, ['name', collection])
    if (!model) {
      throw new Error(`missing collection ${collection}`)
    }

    return modelByName(model)
  }
}

const startPingStrategy = (name, client, db, config, logger) => {
  const interval = Math.max(_.get(config, 'interval', 5000), 5000)
  const threshold = Math.max(_.get(config, 'failureThreshold', 0), 6)

  let isWaiting = false
  let counter = 0

  setInterval(() => {
    if (isWaiting) {
      return
    }
    isWaiting = true
    db.admin().ping((err, result) => {
      const success = _.get(result, 'ok')
      if (err) {
        logger.warn(name, 'ping', err)
      }

      if (success && counter) {
        counter = 0
        listeners(logger).onPingSucceed(name)
      }
      if (!success) {
        listeners(logger).onPingFailed(name)
        counter++
      }

      if (counter >= threshold) {
        counter = 0
        listeners(logger).onPingError(name)
      }

      isWaiting = false
    })
  }, interval)
}

const createConnection = async (name, config, logger) => {
  const {uri, hosts, host, port, db, options, ping} = await parseConfig(config)

  const mongoUri = uri || `mongodb://${hosts || _.join([host, port], ':')}`
  logger.info(`Mongodb connecting to [${mongoUri}]`)
  let dbInstance
  let mongoClient

  try {
    mongoClient = await MongoClient.connect(mongoUri, {
      ...options,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    dbInstance = mongoClient.db(db)

    if (ping) {
      startPingStrategy(name, mongoClient, dbInstance, ping, logger)
    }

    listeners(logger).onConnected(name, dbInstance)
  } catch (err) {
    dbInstance = null
    listeners(logger).onConnectError(name, err)
    throw new Error(err)
  }
  logger.info(`Mongodb connecting to [${mongoUri}], success`)

  return {
    db: dbInstance,
    client: mongoClient,
  }
}

const init = async (ctx, dbConfig, dbModels, eventHandlers) => {
  const {
    instances: {logger},
  } = ctx

  _eventHandlers = {..._eventHandlers, ...eventHandlers}

  const configNames = _.compact(
    _.map(dbConfig, (config, configName) => {
      if (config.type === 'mongodb') {
        return configName
      }
    })
  )
  const connections = {}

  await Bluebird.each(configNames, async (configName) => {
    connections[configName] = await createConnection(
      configName,
      dbConfig[configName],
      logger
    )
  })

  const mongodbByModels = _.pickBy(dbModels, (dbModel, configName) =>
    _.includes(configNames, configName)
  )

  return {
    connections,
    configNames,
    mongoDbByModels: bindMongoUtils(mongodbByModels, connections, dbConfig),
  }
}

const toId = (idObj, increment) => {
  const objectId = new ObjectId(idObj)
  if (!increment) {
    return objectId
  }

  const hexaTime = (+objectId.getTimestamp() + increment * 1000)
    .toString(16)
    .substring(0, 8)

  return new ObjectId(_.replace(idObj, /^([0-9a-f]{8})(.+)$/, hexaTime + '$2'))
}

const execWithTransaction = (conn) => {
  const client = conn.client

  return async (fn, options) => {
    const session = client.startSession()
    try {
      await session.withTransaction(() => fn(session), {
        readPreference: _.get(options, 'readPreference', 'primary'),
        readConcern: _.get(options, 'readConcern', {level: 'local'}),
        writeConcern: _.get(options, 'writeConcern', {w: 'majority'}),
      })

      await session.endSession()
    } catch (err) {
      await session.endSession()

      throw err
    }
  }
}

const bindMongoUtils = (mongodbByModels, connections, dbConfigs) => {
  const mongoDbModels = _.mapValues(mongodbByModels, (dbModel, configName) => {
    const conn = connections[configName]
    const prefix = _.get(dbConfigs, [configName, 'prefix'], '')
    const models = _.keys(dbModel)

    const modelByName = getModel(conn.db, dbModel, prefix)
    const useBycollection = useCollection(conn.db, dbModel, prefix)

    const boundModel = {
      model: modelByName,
      models: (model) => (model ? modelByName(model) : models),
      use: useBycollection,
      get: (collection) => selectCollection(conn.db, collection, prefix),
      toId,
      toIds: (ids) => _.map(_.castArray(ids), (id) => toId(id)),
      withSession: (fn) => conn.client.withSession(fn),
      startSession: () => conn.client.startSession(),
      withTransaction: execWithTransaction(conn),
      conn,
    }

    _.each(dbModel, (model, name) => {
      Object.defineProperty(boundModel, _.upperFirst(_.camelCase(name)), {
        get: () => modelByName(name),
      })
    })

    return Object.freeze(boundModel)
  })

  return mongoDbModels
}

export {createConnection, init, toId}
