import _ from 'lodash'
import {ObjectId} from 'mongodb'

const findAllCreator = (collection) => {
  return function findAll(...args) {
    return collection.find(...args).toArray()
  }
}

export const castIds = (IDS_FIELDS, valueData) =>
  _.mapValues(valueData, (value, key) => {
    if (_.includes(IDS_FIELDS, key)) {
      if (typeof value === 'string' && ObjectId.isValid(value)) {
        return new ObjectId(value)
      }

      if (Array.isArray(value)) {
        return _.map(value, (v) =>
          typeof v === 'string' && ObjectId.isValid(v) ? new ObjectId(v) : v
        )
      }

      if (
        !_.isNil(value) &&
        Object.getPrototypeOf(value) === Object.prototype
      ) {
        return _.mapValues(value, (nestedValue, nestedKey) => {
          if (_.startsWith(nestedKey, '$')) {
            if (
              typeof nestedValue === 'string' &&
              ObjectId.isValid(nestedValue)
            ) {
              return new ObjectId(nestedValue)
            }

            if (Array.isArray(nestedValue)) {
              return _.map(nestedValue, (v) =>
                typeof v === 'string' && ObjectId.isValid(v)
                  ? new ObjectId(v)
                  : v
              )
            }
          }

          return nestedValue
        })
      }
    }

    if (_.startsWith(key, '$')) {
      if (typeof value === 'object') {
        if (Array.isArray(value)) {
          return _.map(value, (nestedValue) => {
            if (typeof nestedValue === 'object') {
              return castIds(IDS_FIELDS, nestedValue)
            }

            return nestedValue
          })
        }

        return castIds(IDS_FIELDS, value)
      }
    }

    return value
  })

const deprecatedFn = (fn) => () => {
  console.error(fn, 'is deprecated')
}

export const bind = (collection, model) => {
  const verbs = [
    'aggregate',
    'bulkWrite',
    'countDocuments',
    'deleteOne',
    'deleteMany',
    'distinct',
    'estimatedDocumentCount',
    'find',
    'findOne',
    'findOneAndDelete',
    'findOneAndReplace',
    'findOneAndUpdate',
    'initializeOrderedBulkOp',
    'initializeUnorderedBulkOp',
    'insertOne',
    'insertMany',
    'replaceOne',
    'stats',
    'updateOne',
    'updateMany',
    'mapReduce',
    'watch',

    'indexes',
    'dropIndex',
    'indexExists',
    'createIndex',
    'drop',
  ]

  const mongoLib = _.fromPairs(
    _.map(verbs, (verb) => [
      verb,
      collection[verb] ? collection[verb].bind(collection) : deprecatedFn(verb),
    ])
  )

  mongoLib.of = mongoLib.from = (query) => castIds(model.castIds || [], query)
  mongoLib.findAll = findAllCreator(collection)

  mongoLib.Collection = collection
  mongoLib.name = collection.s.namespace.collection
  mongoLib.db = collection.s.namespace.db

  return mongoLib
}
