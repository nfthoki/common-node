import {transports, format} from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'

export default async (type, opts, defaultFormats) => {
  switch (type) {
    case 'console':
      return new transports.Console({
        format: format.combine(format.colorize(), ...defaultFormats),
      })
    case 'file':
      return new DailyRotateFile({filename: opts.filename})
    default:
      throw new Error(
        `[logger][transport] no transport type found type=[${type}]`
      )
  }
}
