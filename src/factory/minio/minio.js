import _ from 'lodash'
import * as Minio from 'minio'

const parseConfig = (config) => {
  const parsedConfig = _.cloneDeep(config)

  const portConfig = _.get(parsedConfig, 'client.port')
  if (portConfig) {
    _.set(parsedConfig, 'client.port', parseInt(portConfig))
  }

  return parsedConfig
}

const init = async (ctx, config) => {
  const minio = new Minio.Client(parseConfig(config).client)

  return {minio}
}

export {init}
