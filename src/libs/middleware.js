import _ from 'lodash'
import Moment from 'moment'
import BigNumber from 'bignumber.js'

import {tryNext} from 'libs/request-handler'
import {ClientError} from 'libs/error'
import {isObjectId} from 'libs/mongodb'

export const useString = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useString] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useString] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.trim(_.get(req[source], fieldName))

    if (!value) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    ctx.setData(alias || fieldName, value)
  })
}

export const useObjectId = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useObjectId] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useObjectId] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.trim(_.get(req[source], fieldName))

    if (!value) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    if (!isObjectId(value)) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    ctx.setData(alias || fieldName, value)
  })
}

export const useTimestamp = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useTimestamp] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useTimestamp] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(_.trim(value))) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    const numberValue = Number(value)
    const parsed = Moment.utc(numberValue || value)

    if (!parsed.isValid()) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    ctx.setData(alias || fieldName, parsed.valueOf())
  })
}

export const useInt = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useInt] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useInt] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(_.trim(value))) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    const numberValue = Number(value)
    if (Number.isInteger(numberValue)) {
      ctx.setData(alias || fieldName, numberValue)
    } else {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      } else {
        ctx.setData(alias || fieldName, defaultValue)
      }
    }
  })
}

export const useNumber = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')
  const minValue = _.get(options, 'min')
  const maxValue = _.get(options, 'max')

  if (!fieldName) {
    throw new Error('[useNumber] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useNumber] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(_.trim(value))) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    let numberValue = Number(value)
    if (Number.isNaN(numberValue)) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    if (!_.isNil(maxValue)) {
      numberValue = Math.max(numberValue, minValue)
    }
    if (!_.isNil(maxValue)) {
      numberValue = Math.min(numberValue, maxValue)
    }

    ctx.setData(alias || fieldName, numberValue)
  })
}

export const useBigNumber = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')
  const minValue = _.get(options, 'min')
  const maxValue = _.get(options, 'max')

  if (!fieldName) {
    throw new Error('[useNumber] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useNumber] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(_.trim(value))) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    let numberValue = BigNumber(value)
    if (numberValue.isNaN()) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      }
      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    if (_.isNil(minValue)) {
      numberValue = BigNumber.max(numberValue, minValue)
    }
    if (_.isNil(maxValue)) {
      numberValue = BigNumber.min(numberValue, maxValue)
    }

    ctx.setData(alias || fieldName, numberValue.toString())

    return
  })
}

export const useBoolean = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useBoolean] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useBoolean] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(_.trim(value))) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    const boolValue =
      typeof value === 'string' ? value === 'true' : Boolean(value)

    ctx.setData(alias || fieldName, boolValue)
  })
}

export const useEnum = (fieldName, enumData, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useEnum] fieldName value must be provided')
  }
  if (!enumData) {
    throw new Error('[useEnum] enumData value must be provided')
  }
  if (!source) {
    throw new Error('[useEnum] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(_.trim(value))) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    const found = _.find(enumData, (enumValue) => enumValue === value)
    if (found) {
      ctx.setData(alias || fieldName, value)
    } else {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'invalid'})
      } else {
        ctx.setData(alias || fieldName, defaultValue)
      }
    }
  })
}

export const useArrayEnum = (fieldName, enumData, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useArrayEnum] fieldName value must be provided')
  }
  if (!enumData) {
    throw new Error('[useArrayEnum] enumData value must be provided')
  }
  if (!source) {
    throw new Error('[useArrayEnum] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const arrayValues = _.castArray(_.get(req[source], fieldName) || [])

    if (_.isEmpty(arrayValues)) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    const notExists = (findValue) =>
      !_.find(enumData, (enumValue) => enumValue === findValue)

    if (_.some(arrayValues, notExists)) {
      throw new ClientError({[fieldName]: 'invalid'})
    }

    ctx.setData(alias || fieldName, arrayValues)
  })
}

export const useArray = (fieldName, options) => {
  const isRequired = _.get(options, 'isRequired', false)
  const alias = _.get(options, 'alias', null)
  const source = _.get(options, 'source', null)
  const defaultValue = _.get(options, 'default')

  if (!fieldName) {
    throw new Error('[useArray] fieldName value must be provided')
  }
  if (!source) {
    throw new Error('[useArray] source value must be provided')
  }

  return tryNext((req, res, next, ctx) => {
    const value = _.get(req[source], fieldName)

    if (_.isEmpty(value)) {
      if (isRequired) {
        throw new ClientError({[fieldName]: 'required'})
      }

      ctx.setData(alias || fieldName, defaultValue)

      return
    }

    ctx.setData(alias || fieldName, _.castArray(value || []))
  })
}
