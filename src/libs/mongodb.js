import _ from 'lodash'
import {ObjectId} from 'mongodb'

export const isObjectId = (value) => {
  if (typeof value === 'string' && ObjectId.isValid(value)) {
    return true
  }
}

export const toObjectId = (value) => {
  if (value && isObjectId(value)) {
    return new ObjectId(value)
  }

  return null
}
