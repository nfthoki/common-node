import _ from 'lodash'
import {StatusCodes} from 'http-status-codes'
import {assignObjOnce} from '/libs/object'
import {clientErrorHandler} from '/libs/error'

export const tryProxy = (fnc) =>
  async function tryProxy(req, res, next) {
    try {
      const response = await fnc(req, res, next, req.ctx)

      const data = _.get(response, 'data')
      const status = _.get(response, 'status', 500)

      res.status(status || StatusCodes.INTERNAL_SERVER_ERROR).json(data)
    } catch (error) {
      next(error)
    }
  }

export const tryResponse = (fncs) => {
  const middleWares = _.flattenDeep(_.castArray(fncs))

  return _.map(
    middleWares,
    (fnc, fnIndex) =>
      async function tryResponse(req, res, next) {
        const nextFns = []
        const wrapFn = (res, fn) =>
          function wrapFn(...args) {
            nextFns.push(fn.name)
            fn.call(res, ...args)
          }

        res.json = wrapFn(res, res.json)
        res.send = wrapFn(res, res.send)
        res.end = wrapFn(res, res.end)
        res.success = wrapFn(res, res.success)
        res.fail = wrapFn(res, res.fail)
        res.on = wrapFn(res, res.on)

        const wrappedNext = (...args) => {
          if (!_.isEmpty(args)) {
            nextFns.push('next:error')
          } else {
            nextFns.push('next')
          }
          next(...args)
        }
        const ctx = req.ctx

        try {
          const result = await fnc(req, res, wrappedNext, ctx)
          if (_.isEmpty(nextFns) && fnIndex + 1 === middleWares.length) {
            res.success(result)
          }
        } catch (error) {
          wrappedNext(error)
        }
      }
  )
}

export const tryNext = (fncs) => {
  const middleWares = _.flattenDeep(_.castArray(fncs))

  return _.map(
    middleWares,
    (fnc) =>
      async function tryNext(req, res, next) {
        const nextFns = []
        const wrapFn = (fn) =>
          function wrapFn(...args) {
            nextFns.push(fn.name)
            fn.call(res, ...args)
          }

        const wrappedNext = wrapFn(next)
        const ctx = req.ctx

        try {
          const result = await fnc(req, res, wrappedNext, ctx)
          ctx.set('next', result)
        } catch (error) {
          wrappedNext(error)
        }

        if (_.isEmpty(nextFns)) {
          wrappedNext()
        }
      }
  )
}

export const mergeData = (fncs) => {
  const middleWares = _.flattenDeep(_.castArray(fncs))

  return _.map(
    middleWares,
    (fnc) =>
      async function mergeData(req, res, next) {
        const nextFns = []
        const wrapFn = (fn) =>
          function wrapFn(...args) {
            nextFns.push(fn.name)
            fn.call(res, ...args)
          }

        const wrappedNext = wrapFn(next)
        const ctx = req.ctx

        try {
          const result = await fnc(req, res, wrappedNext, ctx)
          if (!_.isNil(result)) {
            ctx.mergeData(result)
          }
        } catch (error) {
          wrappedNext(error)
        }

        if (_.isEmpty(nextFns)) {
          wrappedNext()
        }
      }
  )
}

export const writeData = (fncs) => {
  const middleWares = _.flattenDeep(_.castArray(fncs))

  return _.map(
    middleWares,
    (fnc) =>
      async function writeData(req, res, next) {
        const nextFns = []
        const wrapFn = (fn) =>
          function wrapFn(...args) {
            nextFns.push(fn.name)
            fn.call(res, ...args)
          }

        const wrappedNext = wrapFn(next)
        const ctx = req.ctx

        try {
          const result = await fnc(req, res, wrappedNext, ctx)
          if (!_.isNil(result)) {
            ctx.data = result
          }
        } catch (error) {
          wrappedNext(error)
        }

        if (_.isEmpty(nextFns)) {
          wrappedNext()
        }
      }
  )
}

export const socketNext = (fncs) => {
  const middleWares = _.flattenDeep(_.castArray(fncs))

  return async function tryNext(socket, next) {
    const req = socket.client.request
    try {
      await Bluebird.each(middleWares, (fnc) => fnc(req))
    } catch (error) {
      return next(error)
    }

    next()
  }
}

export const registerReqContext = (app, ctx) => {
  app.all('*', function contextCreator(req, res, next) {
    req.ctx = assignObjOnce({req, res, _private: {data: {}}}, ctx)

    req.ctx.set = _.partial(_.set, req.ctx._private)
    req.ctx.get = _.partial(_.get, req.ctx._private)
    req.ctx.assign = _.partial(_.assign, req.ctx._private)
    req.ctx.merge = _.partial(_.merge, req.ctx._private)
    req.ctx.isNil = (prop) => _.isNil(_.get(req.ctx._private, prop))

    Object.defineProperties(req.ctx, {
      data: {
        get: () => req.ctx._private.data,
        set: (value) => (req.ctx._private.data = value),
      },
    })

    req.ctx.mergeData = _.partial(_.merge, req.ctx._private.data)
    req.ctx.setData = _.partial(_.set, req.ctx._private.data)

    next()
  })
}

export const registerResContext = (app) => {
  const successCreator = (req, res) =>
    function success(data, options) {
      const metadata = _.get(options, 'metadata')

      const statusCode = _.get(
        options,
        'statusCode',
        res.statusCode || StatusCodes.OK
      )

      res.status(statusCode).json({
        success: true,
        metadata,
        data,
      })
    }

  const failCreator = (req, res) =>
    function fail(statusCode = StatusCodes.BAD_REQUEST, error, errorCodes) {
      res.status(statusCode).json({
        success: false,
        error,
        errorCodes,
      })
    }

  app.all('*', (req, res, next) => {
    res.success = successCreator(req, res)
    res.fail = failCreator(req, res)
    next()
  })
}

export const registerErrorHandler = (app, logger) => {
  app.use(clientErrorHandler, (error, req, res, next) => {
    logger && logger.error(error.stack)
    res.fail(StatusCodes.INTERNAL_SERVER_ERROR, error.message, [
      'INTERNAL_SERVER_ERROR',
    ])
  })
}
