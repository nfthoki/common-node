import path from 'path'
import _ from 'lodash'
import axios from 'axios'
import UrlPattern from 'url-pattern'
import {StatusCodes} from 'http-status-codes'

export const httpFlow = (request) => {
  return async (...params) => {
    try {
      const result = await request(...params)

      return {
        statusText: result.statusText,
        status: result.status,
        data: result.data,
        errcode: result.code,
        config: result.config,
        headers: result.headers,
      }
    } catch (err) {
      const response = _.get(err, 'response', {})

      return {
        statusText: response.statusText,
        status: response.status,
        data: response.data,
        errcode: err.code,
        config: err.config,
        headers: response.headers,
      }
    }
  }
}

export const httpTransformers = [
  (data) => {
    try {
      return JSON.parse(data)
    } catch {
      return data
    }
  },
]

export const httpInternalFlow = (request) => {
  return async (...params) => {
    try {
      const result = await request(...params)

      return {
        statusText: result.statusText,
        status: result.status,
        data: _.isObject(result.data)
          ? result.data
          : {success: result.status === StatusCodes.OK},
        errcode: result.code,
        config: result.config,
        headers: result.headers,
      }
    } catch (err) {
      const response = _.get(err, 'response', {})
      const responsedData = _.isObject(response.data) ? response.data : {}

      return {
        statusText: response.statusText,
        status: response.status,
        data: {success: false, ...responsedData},
        errcode: err.code,
        config: err.config,
        headers: response.headers,
      }
    }
  }
}

export const httpInternalTransformers = [
  (data) => {
    try {
      return _.isEmpty(data) ? null : JSON.parse(data)
    } catch {
      return {success: false}
    }
  },
]

export const httpCreator = (config, handlers = {}) => {
  const {origin, timeout, headers, version, baseUrl} = config
  const {flowController = httpFlow, transformers = httpTransformers} = handlers

  const baseURL =
    baseUrl || _.trimEnd(origin + (version ? path.join('/', version) : ''), '/')

  const httpExecutor = axios.create({
    baseURL,
    timeout,
    headers,
    validateStatus: false,
    transformResponse: transformers,
  })

  const to3Params = (fnc) => {
    return (path, ...params) => {
      if (_.size(params) > 1) {
        return fnc(path, ...params)
      }
      const {data, ...config} = params[0] || {}

      return fnc(path, data, config)
    }
  }

  const http = {
    head: flowController(httpExecutor.head),
    get: flowController(httpExecutor.get),
    options: flowController(httpExecutor.options),
    post: to3Params(flowController(httpExecutor.post)),
    put: to3Params(flowController(httpExecutor.put)),
    patch: to3Params(flowController(httpExecutor.patch)),
    delete: flowController(httpExecutor.delete),
  }

  _.each(_.keys(http), (method) => {
    http[_.toUpper(method)] = http[method]
  })

  return http
}

export const proxyCreator = (proxyConfig, handlers) => {
  const services = _.map(_.castArray(proxyConfig), (config) =>
    httpCreator(config, handlers)
  )

  let requestCount = 0

  const parseHeaders = (req, stream = false) => {
    const headers = _.omit(req.headers, ['host', 'content-length'])
    const contentType = _.toLower(
      _.get(req.headers, 'content-type', 'application/json')
    )
    const method = _.toUpper(req.method)

    if (stream) {
      return headers
    }

    if (!['application/json'].includes(contentType)) {
      return req.headers
    }

    const contentLength = Buffer.byteLength(JSON.stringify(req.body))

    switch (method) {
      case 'POST':
      case 'PUT':
      case 'PATCH':
      case 'DELETE':
        return {...headers, 'content-length': contentLength}
      default:
        return headers
    }
  }

  const parseOptions = (options) => {
    const responseType = _.get(options, 'responseType', 'json')
    const timeout = _.get(options, 'timeout')
    const ovewrite = _.omitBy({timeout, responseType}, _.isNil)

    return ovewrite
  }

  const doForward = (req) => {
    requestCount = (requestCount + 1) % services.length
    const service = services[requestCount]
    const pattern = new UrlPattern(req.url)

    const url = pattern.stringify(req.params)
    const headers = parseHeaders(req)
    const body = req.body
    const method = _.toUpper(req.method)

    switch (method) {
      case 'GET':
        return service.get(url, {data: body, headers})
      case 'POST':
        return service.post(url, body, {headers})
      case 'PUT':
        return service.put(url, body, {headers})
      case 'PATCH':
        return service.patch(url, body, {headers})
      case 'DELETE':
        return service.delete(url, {data: body, headers})
    }
  }

  const doForwardStream = (req) => {
    requestCount = (requestCount + 1) % services.length
    const service = services[requestCount]
    const pattern = new UrlPattern(req.url)

    const url = pattern.stringify(req.params)
    const headers = parseHeaders(req, true)
    const method = _.toUpper(req.method)

    switch (method) {
      case 'GET':
        return service.get(url, {data: req, headers})
      case 'POST':
        return service.post(url, req, {headers})
      case 'PUT':
        return service.put(url, req, {headers})
      case 'PATCH':
        return service.patch(url, body, {headers})
      case 'DELETE':
        return service.delete(url, {data: req, headers})
    }
  }

  const doForwardOptions = (options) => (req) => {
    const prefix = _.get(options, 'prefix', '')
    const stream = _.get(options, 'stream', false)

    requestCount = (requestCount + 1) % services.length
    const service = services[requestCount]
    const targetUrl = _.split(prefix + req.url, '/')
      .filter((i) => i)
      .join('/')

    const pattern = new UrlPattern('/' + targetUrl)

    const url = pattern.stringify(req.params)
    const headers = parseHeaders(req, stream)
    const body = stream ? req : req.body
    const method = _.toUpper(req.method)

    const ovewrite = parseOptions(options)

    switch (method) {
      case 'GET':
        return service.get(url, {data: body, headers, ...ovewrite})
      case 'POST':
        return service.post(url, body, {headers, ...ovewrite})
      case 'PUT':
        return service.put(url, body, {headers, ...ovewrite})
      case 'PATCH':
        return service.patch(url, body, {headers, ...ovewrite})
      case 'DELETE':
        return service.delete(url, {data: body, headers, ...ovewrite})
    }
  }

  const doForwardTo = (forwardUrl, options) => {
    const prefix = _.get(options, 'prefix', '')
    const stream = _.get(options, 'stream', false)

    return (req) => {
      requestCount = (requestCount + 1) % services.length
      const service = services[requestCount]
      const targetUrl = _.split(prefix + (forwardUrl || req.url), '/')
        .filter((i) => i)
        .join('/')

      const pattern = new UrlPattern('/' + targetUrl)

      const url = pattern.stringify(req.params)
      const headers = parseHeaders(req, stream)
      const body = stream ? req : req.body
      const method = _.toUpper(req.method)

      const ovewrite = parseOptions(options)

      switch (method) {
        case 'GET':
          return service.get(url, {data: body, headers, ...ovewrite})
        case 'POST':
          return service.post(url, body, {headers, ...ovewrite})
        case 'PUT':
          return service.put(url, body, {headers, ...ovewrite})
        case 'PATCH':
          return service.patch(url, body, {headers, ...ovewrite})
        case 'DELETE':
          return service.delete(url, {data: body, headers, ...ovewrite})
      }
    }
  }

  return {
    forwardStream: doForwardStream,
    forwardOptions: doForwardOptions,
    forward: doForward,
    forwardTo: doForwardTo,
  }
}

export const pathParser = (source, context) => {
  if (typeof source === 'string') {
    return source
  }

  const fnSource = _.get(source, 'function', '_.stubString')

  try {
    return eval(fnSource)(context)
  } catch (err) {
    return ''
  }
}
