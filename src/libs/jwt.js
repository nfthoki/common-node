import jwt from 'jsonwebtoken'
import Bluebird from 'bluebird'

const asyncVerify = Bluebird.promisify(jwt.verify)
const asyncSign = Bluebird.promisify(jwt.sign)

export const verify = async (token, secret) => {
  try {
    return await asyncVerify(token, secret)
  } catch (error) {
    return null
  }
}

export const sign = async (data, secret, expiresIn) => {
  try {
    return await asyncSign(data, secret, {expiresIn})
  } catch (error) {
    return null
  }
}
