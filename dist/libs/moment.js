"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.zeroMoment = exports.zeroDiff = exports.secOfYear = exports.secOfWeek = void 0;

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ZERODATESTRING = '0001-01-01T00:00:00.000Z';

var zeroMoment = function zeroMoment(diff) {
  var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'second';

  if (diff) {
    return _moment.default.utc(ZERODATESTRING).add(diff, unit);
  }

  return _moment.default.utc(ZERODATESTRING);
};

exports.zeroMoment = zeroMoment;

var zeroDiff = function zeroDiff(data) {
  var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'second';
  return (0, _moment.default)(data).diff(_moment.default.utc(ZERODATESTRING), unit);
};

exports.zeroDiff = zeroDiff;

var secOfWeek = data => {
  return (0, _moment.default)(data).diff(_moment.default.utc().startOf('week'), 'second');
};

exports.secOfWeek = secOfWeek;

var secOfYear = data => {
  return (0, _moment.default)(data).diff(_moment.default.utc().startOf('year'), 'second');
};

exports.secOfYear = secOfYear;