"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.writeData = exports.tryResponse = exports.tryProxy = exports.tryNext = exports.socketNext = exports.registerResContext = exports.registerReqContext = exports.registerErrorHandler = exports.mergeData = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _httpStatusCodes = require("http-status-codes");

var _object = require("./object");

var _error = require("./error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var tryProxy = fnc => /*#__PURE__*/function () {
  var _tryProxy = _asyncToGenerator(function* (req, res, next) {
    try {
      var response = yield fnc(req, res, next, req.ctx);

      var data = _lodash.default.get(response, 'data');

      var status = _lodash.default.get(response, 'status', 500);

      res.status(status || _httpStatusCodes.StatusCodes.INTERNAL_SERVER_ERROR).json(data);
    } catch (error) {
      next(error);
    }
  });

  function tryProxy(_x, _x2, _x3) {
    return _tryProxy.apply(this, arguments);
  }

  return tryProxy;
}();

exports.tryProxy = tryProxy;

var tryResponse = fncs => {
  var middleWares = _lodash.default.flattenDeep(_lodash.default.castArray(fncs));

  return _lodash.default.map(middleWares, (fnc, fnIndex) => /*#__PURE__*/function () {
    var _tryResponse = _asyncToGenerator(function* (req, res, next) {
      var nextFns = [];

      var wrapFn = (res, fn) => function wrapFn() {
        nextFns.push(fn.name);

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        fn.call(res, ...args);
      };

      res.json = wrapFn(res, res.json);
      res.send = wrapFn(res, res.send);
      res.end = wrapFn(res, res.end);
      res.success = wrapFn(res, res.success);
      res.fail = wrapFn(res, res.fail);
      res.on = wrapFn(res, res.on);

      var wrappedNext = function wrappedNext() {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        if (!_lodash.default.isEmpty(args)) {
          nextFns.push('next:error');
        } else {
          nextFns.push('next');
        }

        next(...args);
      };

      var ctx = req.ctx;

      try {
        var result = yield fnc(req, res, wrappedNext, ctx);

        if (_lodash.default.isEmpty(nextFns) && fnIndex + 1 === middleWares.length) {
          res.success(result);
        }
      } catch (error) {
        wrappedNext(error);
      }
    });

    function tryResponse(_x4, _x5, _x6) {
      return _tryResponse.apply(this, arguments);
    }

    return tryResponse;
  }());
};

exports.tryResponse = tryResponse;

var tryNext = fncs => {
  var middleWares = _lodash.default.flattenDeep(_lodash.default.castArray(fncs));

  return _lodash.default.map(middleWares, fnc => /*#__PURE__*/function () {
    var _tryNext = _asyncToGenerator(function* (req, res, next) {
      var nextFns = [];

      var wrapFn = fn => function wrapFn() {
        nextFns.push(fn.name);

        for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
          args[_key3] = arguments[_key3];
        }

        fn.call(res, ...args);
      };

      var wrappedNext = wrapFn(next);
      var ctx = req.ctx;

      try {
        var result = yield fnc(req, res, wrappedNext, ctx);
        ctx.set('next', result);
      } catch (error) {
        wrappedNext(error);
      }

      if (_lodash.default.isEmpty(nextFns)) {
        wrappedNext();
      }
    });

    function tryNext(_x7, _x8, _x9) {
      return _tryNext.apply(this, arguments);
    }

    return tryNext;
  }());
};

exports.tryNext = tryNext;

var mergeData = fncs => {
  var middleWares = _lodash.default.flattenDeep(_lodash.default.castArray(fncs));

  return _lodash.default.map(middleWares, fnc => /*#__PURE__*/function () {
    var _mergeData = _asyncToGenerator(function* (req, res, next) {
      var nextFns = [];

      var wrapFn = fn => function wrapFn() {
        nextFns.push(fn.name);

        for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
          args[_key4] = arguments[_key4];
        }

        fn.call(res, ...args);
      };

      var wrappedNext = wrapFn(next);
      var ctx = req.ctx;

      try {
        var result = yield fnc(req, res, wrappedNext, ctx);

        if (!_lodash.default.isNil(result)) {
          ctx.mergeData(result);
        }
      } catch (error) {
        wrappedNext(error);
      }

      if (_lodash.default.isEmpty(nextFns)) {
        wrappedNext();
      }
    });

    function mergeData(_x10, _x11, _x12) {
      return _mergeData.apply(this, arguments);
    }

    return mergeData;
  }());
};

exports.mergeData = mergeData;

var writeData = fncs => {
  var middleWares = _lodash.default.flattenDeep(_lodash.default.castArray(fncs));

  return _lodash.default.map(middleWares, fnc => /*#__PURE__*/function () {
    var _writeData = _asyncToGenerator(function* (req, res, next) {
      var nextFns = [];

      var wrapFn = fn => function wrapFn() {
        nextFns.push(fn.name);

        for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
          args[_key5] = arguments[_key5];
        }

        fn.call(res, ...args);
      };

      var wrappedNext = wrapFn(next);
      var ctx = req.ctx;

      try {
        var result = yield fnc(req, res, wrappedNext, ctx);

        if (!_lodash.default.isNil(result)) {
          ctx.data = result;
        }
      } catch (error) {
        wrappedNext(error);
      }

      if (_lodash.default.isEmpty(nextFns)) {
        wrappedNext();
      }
    });

    function writeData(_x13, _x14, _x15) {
      return _writeData.apply(this, arguments);
    }

    return writeData;
  }());
};

exports.writeData = writeData;

var socketNext = fncs => {
  var middleWares = _lodash.default.flattenDeep(_lodash.default.castArray(fncs));

  return /*#__PURE__*/function () {
    var _tryNext2 = _asyncToGenerator(function* (socket, next) {
      var req = socket.client.request;

      try {
        yield Bluebird.each(middleWares, fnc => fnc(req));
      } catch (error) {
        return next(error);
      }

      next();
    });

    function tryNext(_x16, _x17) {
      return _tryNext2.apply(this, arguments);
    }

    return tryNext;
  }();
};

exports.socketNext = socketNext;

var registerReqContext = (app, ctx) => {
  app.all('*', function contextCreator(req, res, next) {
    req.ctx = (0, _object.assignObjOnce)({
      req,
      res,
      _private: {
        data: {}
      }
    }, ctx);
    req.ctx.set = _lodash.default.partial(_lodash.default.set, req.ctx._private);
    req.ctx.get = _lodash.default.partial(_lodash.default.get, req.ctx._private);
    req.ctx.assign = _lodash.default.partial(_lodash.default.assign, req.ctx._private);
    req.ctx.merge = _lodash.default.partial(_lodash.default.merge, req.ctx._private);

    req.ctx.isNil = prop => _lodash.default.isNil(_lodash.default.get(req.ctx._private, prop));

    Object.defineProperties(req.ctx, {
      data: {
        get: () => req.ctx._private.data,
        set: value => req.ctx._private.data = value
      }
    });
    req.ctx.mergeData = _lodash.default.partial(_lodash.default.merge, req.ctx._private.data);
    req.ctx.setData = _lodash.default.partial(_lodash.default.set, req.ctx._private.data);
    next();
  });
};

exports.registerReqContext = registerReqContext;

var registerResContext = app => {
  var successCreator = (req, res) => function success(data, options) {
    var metadata = _lodash.default.get(options, 'metadata');

    var statusCode = _lodash.default.get(options, 'statusCode', res.statusCode || _httpStatusCodes.StatusCodes.OK);

    res.status(statusCode).json({
      success: true,
      metadata,
      data
    });
  };

  var failCreator = (req, res) => function fail() {
    var statusCode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _httpStatusCodes.StatusCodes.BAD_REQUEST;
    var error = arguments.length > 1 ? arguments[1] : undefined;
    var errorCodes = arguments.length > 2 ? arguments[2] : undefined;
    res.status(statusCode).json({
      success: false,
      error,
      errorCodes
    });
  };

  app.all('*', (req, res, next) => {
    res.success = successCreator(req, res);
    res.fail = failCreator(req, res);
    next();
  });
};

exports.registerResContext = registerResContext;

var registerErrorHandler = (app, logger) => {
  app.use(_error.clientErrorHandler, (error, req, res, next) => {
    logger && logger.error(error.stack);
    res.fail(_httpStatusCodes.StatusCodes.INTERNAL_SERVER_ERROR, error.message, ['INTERNAL_SERVER_ERROR']);
  });
};

exports.registerErrorHandler = registerErrorHandler;