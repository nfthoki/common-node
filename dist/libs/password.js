"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.legacyHash = exports.hash = exports.createSalt = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _crypto = _interopRequireDefault(require("crypto"));

var _bluebird = _interopRequireDefault(require("bluebird"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var asyncPbkdf2 = _bluebird.default.promisify(_crypto.default.pbkdf2);

var hash = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (password, salt) {
    var encoding = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'base64';
    var digest = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'sha512';
    var hashedBuffer = yield asyncPbkdf2(password, salt, 99139, 128, digest);
    return hashedBuffer.toString(encoding);
  });

  return function hash(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.hash = hash;

var legacyHash = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (password, salt) {
    var saltBuf = Buffer.from(salt, 'base64');
    var hashedBuffer = yield asyncPbkdf2(password, saltBuf, 10000, 64, 'sha1');
    return hashedBuffer.toString('base64');
  });

  return function legacyHash(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.legacyHash = legacyHash;

var createSalt = function createSalt() {
  var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 128;
  var pattern = '1234567890!@#$%^&*()abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXYZ';
  return _lodash.default.times(_lodash.default.max([length, 32]), () => pattern[_lodash.default.floor(Math.random() * pattern.length)]).join('');
};

exports.createSalt = createSalt;