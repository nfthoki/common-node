"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toObjectId = exports.isObjectId = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _mongodb = require("mongodb");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isObjectId = value => {
  if (typeof value === 'string' && _mongodb.ObjectId.isValid(value)) {
    return true;
  }
};

exports.isObjectId = isObjectId;

var toObjectId = value => {
  if (value && isObjectId(value)) {
    return new _mongodb.ObjectId(value);
  }

  return null;
};

exports.toObjectId = toObjectId;