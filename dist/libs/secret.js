"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.read = exports.encryptJSON = exports.decryptJSON = void 0;

var _crypto = _interopRequireDefault(require("crypto"));

var _lodash = _interopRequireDefault(require("lodash"));

var _file = require("./file");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var ALGORITHM = 'aes-256-cbc';
var KEY_LENGTH = 32;
var IV_LENGTH = 16;
var KEY_PATTERN = 'abcdefghiklmnopqrstuvwxyz1234567890!@#$%^&*()ABCDEFGHIKLMNOPQRSTUVWXYZ`;,.[]{}-=+_?';
var IV_PATTERN = '1234567890abcdefghiklmnopqrstuvwxyz!@#$%^&*()`;,.[]{}-=+_?ABCDEFGHIKLMNOPQRSTUVWXYZ';

var toLength = (PATTERN, rawKey, LENGTH) => {
  var key = _lodash.default.toString(rawKey).substring(0, LENGTH);

  var currentCount = 0;

  for (var i = 0; i < key.length; i++) {
    currentCount += key.charCodeAt(i);
  }

  for (var _i = key.length; _i <= LENGTH; _i++) {
    var newChar = PATTERN[currentCount % PATTERN.length];
    key += newChar;
    currentCount += newChar.charCodeAt(0) * currentCount;
  }

  return Buffer.from(key).toString('base64').substring(0, LENGTH);
};

var encryptJSON = function encryptJSON() {
  var jsonData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var key = arguments.length > 1 ? arguments[1] : undefined;
  var iv = arguments.length > 2 ? arguments[2] : undefined;

  try {
    var cipher = _crypto.default.createCipheriv(ALGORITHM, Buffer.from(toLength(key, KEY_PATTERN, KEY_LENGTH)), Buffer.from(toLength(iv || key, IV_PATTERN, IV_LENGTH)));

    var encryptedData = Buffer.concat([cipher.update(JSON.stringify(jsonData)), cipher.final()]).toString('hex');
    return _lodash.default.map(_lodash.default.chunk(encryptedData, Math.ceil(Math.sqrt(encryptedData.length))), str => _lodash.default.join(str, ''));
  } catch (error) {
    throw error;
  }
};

exports.encryptJSON = encryptJSON;

var decryptJSON = function decryptJSON(encryptedData, key) {
  var iv = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  try {
    var decipher = _crypto.default.createDecipheriv(ALGORITHM, Buffer.from(toLength(key, KEY_PATTERN, KEY_LENGTH)), Buffer.from(toLength(iv || key, IV_PATTERN, IV_LENGTH)));

    var decrypted = Buffer.concat([decipher.update(_lodash.default.join(encryptedData, ''), 'hex'), decipher.final()]).toString();
    return JSON.parse(decrypted);
  } catch (error) {
    throw error;
  }
};

exports.decryptJSON = decryptJSON;

var read = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (file) {
    var content = yield (0, _file.readAsync)(file);
    return _lodash.default.trim(content, '\n');
  });

  return function read(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.read = read;