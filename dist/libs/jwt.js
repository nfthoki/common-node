"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.verify = exports.sign = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _bluebird = _interopRequireDefault(require("bluebird"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var asyncVerify = _bluebird.default.promisify(_jsonwebtoken.default.verify);

var asyncSign = _bluebird.default.promisify(_jsonwebtoken.default.sign);

var verify = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (token, secret) {
    try {
      return yield asyncVerify(token, secret);
    } catch (error) {
      return null;
    }
  });

  return function verify(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.verify = verify;

var sign = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (data, secret, expiresIn) {
    try {
      return yield asyncSign(data, secret, {
        expiresIn
      });
    } catch (error) {
      return null;
    }
  });

  return function sign(_x3, _x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}();

exports.sign = sign;