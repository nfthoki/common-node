"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.writeFileAsync = exports.readJsonSync = exports.readJsonAsync = exports.readAsync = exports.ensureDir = exports.appendFileAsync = void 0;

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var readJsonAsync = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (fileName) {
    var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    try {
      return yield _fsExtra.default.readJson(fileName);
    } catch (err) {
      return defaultValue;
    }
  });

  return function readJsonAsync(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.readJsonAsync = readJsonAsync;

var readJsonSync = function readJsonSync(fileName) {
  var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  try {
    return _fsExtra.default.readJsonSync(fileName);
  } catch (err) {
    return defaultValue;
  }
};

exports.readJsonSync = readJsonSync;

var writeFileAsync = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (fileName, data) {
    yield ensureDir(fileName);
    return yield _fsExtra.default.writeFile(fileName, data, {
      flag: 'w'
    });
  });

  return function writeFileAsync(_x2, _x3) {
    return _ref2.apply(this, arguments);
  };
}();

exports.writeFileAsync = writeFileAsync;

var appendFileAsync = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (fileName, data) {
    yield ensureDir(fileName);
    return yield _fsExtra.default.appendFile(fileName, data, {
      flag: 'a'
    });
  });

  return function appendFileAsync(_x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.appendFileAsync = appendFileAsync;

var readAsync = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(function* (fileName) {
    try {
      var content = yield _fsExtra.default.readFile(fileName);
      return content.toString();
    } catch (error) {
      return null;
    }
  });

  return function readAsync(_x6) {
    return _ref4.apply(this, arguments);
  };
}();

exports.readAsync = readAsync;

var ensureDir = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator(function* (fileName) {
    yield _fsExtra.default.ensureDir(_path.default.dirname(fileName));
  });

  return function ensureDir(_x7) {
    return _ref5.apply(this, arguments);
  };
}();

exports.ensureDir = ensureDir;