"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.proxyCreator = exports.pathParser = exports.httpTransformers = exports.httpInternalTransformers = exports.httpInternalFlow = exports.httpFlow = exports.httpCreator = void 0;

var _path = _interopRequireDefault(require("path"));

var _lodash = _interopRequireDefault(require("lodash"));

var _axios = _interopRequireDefault(require("axios"));

var _urlPattern = _interopRequireDefault(require("url-pattern"));

var _httpStatusCodes = require("http-status-codes");

var _excluded = ["data"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var httpFlow = request => {
  return /*#__PURE__*/_asyncToGenerator(function* () {
    try {
      var result = yield request(...arguments);
      return {
        statusText: result.statusText,
        status: result.status,
        data: result.data,
        errcode: result.code,
        config: result.config,
        headers: result.headers
      };
    } catch (err) {
      var response = _lodash.default.get(err, 'response', {});

      return {
        statusText: response.statusText,
        status: response.status,
        data: response.data,
        errcode: err.code,
        config: err.config,
        headers: response.headers
      };
    }
  });
};

exports.httpFlow = httpFlow;
var httpTransformers = [data => {
  try {
    return JSON.parse(data);
  } catch (_unused) {
    return data;
  }
}];
exports.httpTransformers = httpTransformers;

var httpInternalFlow = request => {
  return /*#__PURE__*/_asyncToGenerator(function* () {
    try {
      var result = yield request(...arguments);
      return {
        statusText: result.statusText,
        status: result.status,
        data: _lodash.default.isObject(result.data) ? result.data : {
          success: result.status === _httpStatusCodes.StatusCodes.OK
        },
        errcode: result.code,
        config: result.config,
        headers: result.headers
      };
    } catch (err) {
      var response = _lodash.default.get(err, 'response', {});

      var responsedData = _lodash.default.isObject(response.data) ? response.data : {};
      return {
        statusText: response.statusText,
        status: response.status,
        data: _objectSpread({
          success: false
        }, responsedData),
        errcode: err.code,
        config: err.config,
        headers: response.headers
      };
    }
  });
};

exports.httpInternalFlow = httpInternalFlow;
var httpInternalTransformers = [data => {
  try {
    return _lodash.default.isEmpty(data) ? null : JSON.parse(data);
  } catch (_unused2) {
    return {
      success: false
    };
  }
}];
exports.httpInternalTransformers = httpInternalTransformers;

var httpCreator = function httpCreator(config) {
  var handlers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var {
    origin,
    timeout,
    headers,
    version,
    baseUrl
  } = config;
  var {
    flowController = httpFlow,
    transformers = httpTransformers
  } = handlers;

  var baseURL = baseUrl || _lodash.default.trimEnd(origin + (version ? _path.default.join('/', version) : ''), '/');

  var httpExecutor = _axios.default.create({
    baseURL,
    timeout,
    headers,
    validateStatus: false,
    transformResponse: transformers
  });

  var to3Params = fnc => {
    return function (path) {
      for (var _len = arguments.length, params = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        params[_key - 1] = arguments[_key];
      }

      if (_lodash.default.size(params) > 1) {
        return fnc(path, ...params);
      }

      var _ref3 = params[0] || {},
          {
        data
      } = _ref3,
          config = _objectWithoutProperties(_ref3, _excluded);

      return fnc(path, data, config);
    };
  };

  var http = {
    head: flowController(httpExecutor.head),
    get: flowController(httpExecutor.get),
    options: flowController(httpExecutor.options),
    post: to3Params(flowController(httpExecutor.post)),
    put: to3Params(flowController(httpExecutor.put)),
    patch: to3Params(flowController(httpExecutor.patch)),
    delete: flowController(httpExecutor.delete)
  };

  _lodash.default.each(_lodash.default.keys(http), method => {
    http[_lodash.default.toUpper(method)] = http[method];
  });

  return http;
};

exports.httpCreator = httpCreator;

var proxyCreator = (proxyConfig, handlers) => {
  var services = _lodash.default.map(_lodash.default.castArray(proxyConfig), config => httpCreator(config, handlers));

  var requestCount = 0;

  var parseHeaders = function parseHeaders(req) {
    var stream = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var headers = _lodash.default.omit(req.headers, ['host', 'content-length']);

    var contentType = _lodash.default.toLower(_lodash.default.get(req.headers, 'content-type', 'application/json'));

    var method = _lodash.default.toUpper(req.method);

    if (stream) {
      return headers;
    }

    if (!['application/json'].includes(contentType)) {
      return req.headers;
    }

    var contentLength = Buffer.byteLength(JSON.stringify(req.body));

    switch (method) {
      case 'POST':
      case 'PUT':
      case 'PATCH':
      case 'DELETE':
        return _objectSpread(_objectSpread({}, headers), {}, {
          'content-length': contentLength
        });

      default:
        return headers;
    }
  };

  var parseOptions = options => {
    var responseType = _lodash.default.get(options, 'responseType', 'json');

    var timeout = _lodash.default.get(options, 'timeout');

    var ovewrite = _lodash.default.omitBy({
      timeout,
      responseType
    }, _lodash.default.isNil);

    return ovewrite;
  };

  var doForward = req => {
    requestCount = (requestCount + 1) % services.length;
    var service = services[requestCount];
    var pattern = new _urlPattern.default(req.url);
    var url = pattern.stringify(req.params);
    var headers = parseHeaders(req);
    var body = req.body;

    var method = _lodash.default.toUpper(req.method);

    switch (method) {
      case 'GET':
        return service.get(url, {
          data: body,
          headers
        });

      case 'POST':
        return service.post(url, body, {
          headers
        });

      case 'PUT':
        return service.put(url, body, {
          headers
        });

      case 'PATCH':
        return service.patch(url, body, {
          headers
        });

      case 'DELETE':
        return service.delete(url, {
          data: body,
          headers
        });
    }
  };

  var doForwardStream = req => {
    requestCount = (requestCount + 1) % services.length;
    var service = services[requestCount];
    var pattern = new _urlPattern.default(req.url);
    var url = pattern.stringify(req.params);
    var headers = parseHeaders(req, true);

    var method = _lodash.default.toUpper(req.method);

    switch (method) {
      case 'GET':
        return service.get(url, {
          data: req,
          headers
        });

      case 'POST':
        return service.post(url, req, {
          headers
        });

      case 'PUT':
        return service.put(url, req, {
          headers
        });

      case 'PATCH':
        return service.patch(url, body, {
          headers
        });

      case 'DELETE':
        return service.delete(url, {
          data: req,
          headers
        });
    }
  };

  var doForwardOptions = options => req => {
    var prefix = _lodash.default.get(options, 'prefix', '');

    var stream = _lodash.default.get(options, 'stream', false);

    requestCount = (requestCount + 1) % services.length;
    var service = services[requestCount];

    var targetUrl = _lodash.default.split(prefix + req.url, '/').filter(i => i).join('/');

    var pattern = new _urlPattern.default('/' + targetUrl);
    var url = pattern.stringify(req.params);
    var headers = parseHeaders(req, stream);
    var body = stream ? req : req.body;

    var method = _lodash.default.toUpper(req.method);

    var ovewrite = parseOptions(options);

    switch (method) {
      case 'GET':
        return service.get(url, _objectSpread({
          data: body,
          headers
        }, ovewrite));

      case 'POST':
        return service.post(url, body, _objectSpread({
          headers
        }, ovewrite));

      case 'PUT':
        return service.put(url, body, _objectSpread({
          headers
        }, ovewrite));

      case 'PATCH':
        return service.patch(url, body, _objectSpread({
          headers
        }, ovewrite));

      case 'DELETE':
        return service.delete(url, _objectSpread({
          data: body,
          headers
        }, ovewrite));
    }
  };

  var doForwardTo = (forwardUrl, options) => {
    var prefix = _lodash.default.get(options, 'prefix', '');

    var stream = _lodash.default.get(options, 'stream', false);

    return req => {
      requestCount = (requestCount + 1) % services.length;
      var service = services[requestCount];

      var targetUrl = _lodash.default.split(prefix + (forwardUrl || req.url), '/').filter(i => i).join('/');

      var pattern = new _urlPattern.default('/' + targetUrl);
      var url = pattern.stringify(req.params);
      var headers = parseHeaders(req, stream);
      var body = stream ? req : req.body;

      var method = _lodash.default.toUpper(req.method);

      var ovewrite = parseOptions(options);

      switch (method) {
        case 'GET':
          return service.get(url, _objectSpread({
            data: body,
            headers
          }, ovewrite));

        case 'POST':
          return service.post(url, body, _objectSpread({
            headers
          }, ovewrite));

        case 'PUT':
          return service.put(url, body, _objectSpread({
            headers
          }, ovewrite));

        case 'PATCH':
          return service.patch(url, body, _objectSpread({
            headers
          }, ovewrite));

        case 'DELETE':
          return service.delete(url, _objectSpread({
            data: body,
            headers
          }, ovewrite));
      }
    };
  };

  return {
    forwardStream: doForwardStream,
    forwardOptions: doForwardOptions,
    forward: doForward,
    forwardTo: doForwardTo
  };
};

exports.proxyCreator = proxyCreator;

var pathParser = (source, context) => {
  if (typeof source === 'string') {
    return source;
  }

  var fnSource = _lodash.default.get(source, 'function', '_.stubString');

  try {
    return eval(fnSource)(context);
  } catch (err) {
    return '';
  }
};

exports.pathParser = pathParser;