"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jestContext = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var jestContext = function jestContext() {
  var ctx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var context = _lodash.default.cloneDeep(ctx);

  context.__proto__.mockResolvedValue = function (path, value) {
    var fn = jest.fn().mockResolvedValue(value);

    _lodash.default.set(this, path, fn);

    return fn;
  };

  context.__proto__.mockRejectedValue = function (path, value) {
    var fn = jest.fn().mockRejectedValue(value);

    _lodash.default.set(this, path, fn);

    return fn;
  };

  context.__proto__.mockReturnValue = function (path, value) {
    var fn = jest.fn().mockReturnValue(value);

    _lodash.default.set(this, path, fn);

    return fn;
  };

  context.__proto__.mockImplementation = function (path) {
    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var fn = jest.fn().mockImplementation(...args);

    _lodash.default.set(this, path, fn);

    return fn;
  };

  return context;
};

exports.jestContext = jestContext;