"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = exports.fromMongodb = void 0;

var _agenda = _interopRequireDefault(require("agenda"));

var _agendash = _interopRequireDefault(require("agendash"));

var _mongodb = _interopRequireDefault(require("../dbs/mongodb"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var init = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx) {
    var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var {
      instances: {
        logger
      }
    } = ctx;
    var {
      database,
      options
    } = config;
    var {
      db: mongoInstance
    } = yield _mongodb.default.createConnection('agenda', database, logger);
    var result = yield fromMongodb(ctx, mongoInstance, options);
    return result;
  });

  return function init(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.init = init;

var fromMongodb = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (ctx, mongoInstance, options) {
    var agenda = new _agenda.default(_objectSpread(_objectSpread({}, options), {}, {
      mongo: mongoInstance
    }));
    var agendash = (0, _agendash.default)(agenda);
    return {
      agenda,
      agendash
    };
  });

  return function fromMongodb(_x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.fromMongodb = fromMongodb;