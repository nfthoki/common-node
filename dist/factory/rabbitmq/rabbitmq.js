"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _amqplib = _interopRequireDefault(require("amqplib"));

var _bluebird = _interopRequireDefault(require("bluebird"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var defineQueue = (ctx, exchanges) => {
  var {
    instances: {
      logger
    }
  } = ctx;

  var mappedQueue = _lodash.default.flatMap(exchanges, exchange => _lodash.default.filter(_lodash.default.castArray(_lodash.default.get(exchange, 'queues', [])), queue => queue.consumer).map(queue => [queue.consumer.key, queue]));

  var consumerKeys = _lodash.default.map(mappedQueue, 0);

  if (_lodash.default.uniq(consumerKeys).length !== consumerKeys.length) {
    logger.error('[rabbitmq] consumer key must be uniq', consumerKeys);
    throw new Error('[rabbitmq] consumer key must be uniq');
  }

  return _lodash.default.fromPairs(mappedQueue);
};

var defineExchange = (ctx, exchanges) => {
  var {
    instances: {
      logger
    }
  } = ctx;

  var mappedExchange = _lodash.default.filter(exchanges, exchange => exchange.publisher).map(exchange => [exchange.publisher.key, exchange]);

  var publisherKeys = _lodash.default.map(mappedExchange, 0);

  if (_lodash.default.uniq(publisherKeys).length !== publisherKeys.length) {
    logger.error('[rabbitmq] consumer key must be uniq', publisherKeys);
    throw new Error('[rabbitmq] consumer key must be uniq');
  }

  return _lodash.default.fromPairs(mappedExchange);
};

var createConsumer = (ctx, queueKey, channel) => {
  var {
    instances: {
      logger
    }
  } = ctx;
  return function consumer(queue, handler, options) {
    var ackQueue = typeof queue === 'object' ? queue : _lodash.default.find(queueKey, ['name', queue]);

    var ackQueueOptions = _lodash.default.get(ackQueue, 'consumer.options', {});

    var queueOptions = _objectSpread(_objectSpread({}, ackQueueOptions), options);

    var queueName = _lodash.default.get(ackQueue, 'name', _lodash.default.toString(queue));

    return channel.consume(queueName, /*#__PURE__*/function () {
      var _ref = _asyncToGenerator(function* (message) {
        try {
          var exchange = _lodash.default.get(message, 'fields.exchange');

          var routingKey = _lodash.default.get(message, 'fields.routingKey');

          var data = JSON.parse(message.content.toString());

          var isAck = _lodash.default.get(queueOptions, 'noAck', false);

          var ackMessage = () => {
            if (!isAck) {
              isAck = true;
              channel.ack(message);
            }
          };

          var result = yield handler(data, {
            exchange,
            routingKey
          }, ackMessage);

          if (result && !isAck) {
            ackMessage();
          }
        } catch (err) {
          logger.error('[rabbitmq] consume error', err);
        }
      });

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }(), queueOptions);
  };
};

var createPublisher = (ctx, exchangeKey, channel) => {
  var {
    instances: {
      logger
    }
  } = ctx;
  return /*#__PURE__*/function () {
    var _publisher = _asyncToGenerator(function* (exchange, route, message, options) {
      var routingKey = typeof route === 'object' ? _lodash.default.get(route, 'name', '') : route;
      var exchageName = typeof exchange === 'object' ? _lodash.default.get(exchange, 'name') : exchange;

      var foundExchange = _lodash.default.find(exchangeKey, ['name', exchageName]);

      if (!foundExchange) {
        logger.error('[rabbitmq] exchange invalid', exchange);
        return;
      }

      var defaultOptions = _lodash.default.get(foundExchange, 'messageOptions', {});

      var messageContent = Buffer.from(typeof message === 'string' ? message : JSON.stringify(message));
      channel.publish(foundExchange.name, routingKey, messageContent, _objectSpread(_objectSpread({}, defaultOptions), options));
    });

    function publisher(_x2, _x3, _x4, _x5) {
      return _publisher.apply(this, arguments);
    }

    return publisher;
  }();
};

var bindRabbitUtils = (ctx, exchanges, channel) => {
  var queue = defineQueue(ctx, exchanges);
  var exchange = defineExchange(ctx, exchanges);
  var boundModel = {
    channel,
    queue,
    exchange,
    publish: createPublisher(ctx, exchange, channel),
    consume: createConsumer(ctx, queue, channel)
  };
  return boundModel;
};

var init = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (ctx, rabbitmq) {
    var {
      instances: {
        logger
      }
    } = ctx;

    var connection = _lodash.default.get(rabbitmq, 'connection');

    var exchanges = _lodash.default.castArray(_lodash.default.get(rabbitmq, 'exchanges', []));

    if (!connection) {
      logger.error('[rabbitmq] missing rabbitmq config');
      throw new Error('[rabbitmq] missing rabbitmq config');
    }

    var {
      hostname,
      vhost,
      port
    } = connection;
    logger.info("RabbitMQ connecting to [".concat(hostname, ":").concat(port, "][").concat(vhost, "]"));
    var conn = yield _amqplib.default.connect(connection);
    var channel = yield conn.createChannel();
    yield _bluebird.default.each(exchanges, /*#__PURE__*/function () {
      var _ref3 = _asyncToGenerator(function* (exchange) {
        yield channel.assertExchange(exchange.name, exchange.type, exchange.options);

        var queues = _lodash.default.get(exchange, 'queues', []);

        yield _bluebird.default.each(_lodash.default.castArray(queues), /*#__PURE__*/function () {
          var _ref4 = _asyncToGenerator(function* (queue) {
            yield channel.assertQueue(queue.name, queue.options);
            yield channel.bindQueue(queue.name, exchange.name, _lodash.default.isNil(queue.routingKey) ? queue.routingKey || queue.name : queue.routingKey);
          });

          return function (_x9) {
            return _ref4.apply(this, arguments);
          };
        }());
      });

      return function (_x8) {
        return _ref3.apply(this, arguments);
      };
    }());
    logger.info("RabbitMQ connecting to [".concat(hostname, ":").concat(port, "][").concat(vhost, "], success"));
    return {
      rabbitmq: bindRabbitUtils(ctx, exchanges, channel)
    };
  });

  return function init(_x6, _x7) {
    return _ref2.apply(this, arguments);
  };
}();

exports.init = init;