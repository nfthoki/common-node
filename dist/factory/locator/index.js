"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _locator = require("./locator");

var _default = {
  init: _locator.init
};
exports.default = _default;