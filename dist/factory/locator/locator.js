"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.service = exports.init = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _http = require("../../libs/http");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var parseUrls = origin => {
  if (typeof origin === 'string' && _lodash.default.includes(origin, ',')) {
    return _lodash.default.split(origin, ',').filter(i => i);
  }

  return _lodash.default.castArray(origin || []);
};

var init = (ctx, serviceConfig) => {
  var createPool = (serviceConfig, name) => {
    var config = _lodash.default.get(serviceConfig, name);

    if (!config) {
      throw new Error("No service config found for target [".concat(name, "]"));
    }

    var pool = _lodash.default.concat(_lodash.default.map(parseUrls(config.baseUrl), baseUrl => _objectSpread(_objectSpread({}, config), {}, {
      baseUrl
    })), _lodash.default.map(parseUrls(config.origin), origin => _objectSpread(_objectSpread({}, config), {}, {
      origin
    })));

    if (_lodash.default.isEmpty(pool)) {
      throw new Error("Service config pool empty for target [".concat(name, "]"));
    }

    return pool;
  };

  var locator = {
    locate: name => {
      var config = _lodash.default.get(serviceConfig, name);

      if (!config) {
        throw new Error("No service config found for target [".concat(name, "]"));
      }

      return (0, _http.httpCreator)(config, {
        flowController: _http.httpInternalFlow,
        transformers: _http.httpInternalTransformers
      });
    },
    nativeLocate: name => {
      var config = _lodash.default.get(serviceConfig, name);

      if (!config) {
        throw new Error("No service config found for target [".concat(name, "]"));
      }

      return (0, _http.httpCreator)(config, {
        flowController: _http.httpFlow,
        transformers: _http.httpTransformers
      });
    },
    proxyTo: name => {
      var pool = createPool(serviceConfig, name);
      return (0, _http.proxyCreator)(pool, {
        flowController: _http.httpInternalFlow,
        transformers: _http.httpInternalTransformers
      });
    }
  };
  return {
    locator
  };
};

exports.init = init;

var service = (ctx, serviceConfig) => {
  var locator = {
    locate: (() => {
      var config = serviceConfig;

      if (!config) {
        throw new Error("No service config found for target");
      }

      return (0, _http.httpCreator)(config, {
        flowController: _http.httpInternalFlow,
        transformers: _http.httpInternalTransformers
      });
    })(),
    nativeLocate: (() => {
      var config = serviceConfig;

      if (!config) {
        throw new Error("No service config found for target");
      }

      return (0, _http.httpCreator)(config, {
        flowController: _http.httpFlow,
        transformers: _http.httpTransformers
      });
    })(),
    proxyTo: (() => {
      var config = serviceConfig;

      if (!config) {
        throw new Error("No service config found for proxier");
      }

      return (0, _http.proxyCreator)(config, {
        flowController: _http.httpInternalFlow,
        transformers: _http.httpInternalTransformers
      });
    })()
  };
  return locator;
};

exports.service = service;