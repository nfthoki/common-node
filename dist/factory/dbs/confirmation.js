"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.confirmation = void 0;

var _bluebird = _interopRequireDefault(require("bluebird"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var confirmation = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, info) {
    var seconds = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 5;
    var {
      instances: {
        logger
      }
    } = ctx;
    var counter = 0;
    var waitingInSeconds = process.env.IGNORE_SCRIPT_CONFIRMATION ? 0 : seconds;
    logger.warn(info.name);
    logger.warn(info.warning);
    logger.warn(info.params);
    return new _bluebird.default(resolve => {
      var confirmInterval = setInterval(() => {
        counter++;

        if (counter < waitingInSeconds) {
          logger.warn("Performing ".concat(info.name, " in ").concat(waitingInSeconds - counter, " seconds..."));
          return;
        }

        clearInterval(confirmInterval);
        logger.warn("Performing ".concat(info.name, "..."));
        resolve();
      }, 1000);
    });
  });

  return function confirmation(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.confirmation = confirmation;