"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _path = _interopRequireDefault(require("path"));

var _config = require("../../../libs/config");

var _object = require("../../../libs/object");

var _ = _interopRequireDefault(require("./.."));

var _logger = _interopRequireDefault(require("../../logger"));

var _migrate = require("../mongodb/migrate");

var _confirmation = require("../confirmation");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var exec = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* () {
    var rawConfig = require(_path.default.join(global.MAIN_DIR, 'config')).default;

    var config = yield (0, _config.parse)(rawConfig);
    var {
      logger
    } = yield _logger.default.init({
      config
    });
    var ctx = (0, _object.assignObjOnce)({}, {
      config,
      instances: {
        logger
      }
    });
    var dbConfig = ctx.config.databases;
    var databases = yield _.default.init(ctx, dbConfig);
    (0, _object.assignObjOnce)(ctx.instances, databases);
    var targetDb = process.env.SCRIPT_MIGRATE_DB_NAME || 'primary';
    var scriptName = process.env.SCRIPT_MIGRATE_NAME;

    if (!dbConfig[targetDb]) {
      throw new Error("No config for [".concat(targetDb, "]"));
    }

    yield (0, _confirmation.confirmation)(ctx, {
      name: 'MIGRATION SCRIPT',
      warning: 'Press Control + C for cancelling',
      params: {
        targetDb,
        scriptName: scriptName || '<all>'
      }
    });

    if (dbConfig[targetDb].type === 'mongodb') {
      yield (0, _migrate.migrate)(ctx, databases.dbs, targetDb, scriptName);
    }
  });

  return function exec() {
    return _ref.apply(this, arguments);
  };
}();

var _default = exec().catch(console.error);

exports.default = _default;