"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _path = _interopRequireDefault(require("path"));

var _glob = _interopRequireDefault(require("glob"));

var _mongodb = _interopRequireDefault(require("./mongodb"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var loadDaos = (ctx, dbs) => {
  var {
    instances: {
      logger
    }
  } = ctx;

  var daoFiles = _glob.default.sync(_path.default.join(global.MAIN_DIR, 'daos/*.js'), {
    dot: true
  });

  return _lodash.default.fromPairs(_lodash.default.map(daoFiles, filePath => {
    var daoFns = require(filePath);

    var {
      name: filename
    } = _path.default.parse(filePath);

    var daoName = _lodash.default.upperFirst(_lodash.default.camelCase(filename));

    logger.info("-> dao: ".concat(daoName));
    return [daoName, _lodash.default.mapValues(daoFns, fn => _lodash.default.partial(fn, _objectSpread(_objectSpread({}, ctx), {}, {
      db: dbs.primary,
      dbs
    })))];
  }));
};

var init = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, dbConfig, listeners) {
    var dbModels = require(_path.default.join(global.MAIN_DIR, 'models')).default;

    var mongodb = yield _mongodb.default.init(ctx, dbConfig, dbModels, listeners);

    var dbs = _objectSpread({}, mongodb.mongoDbByModels);

    return {
      $: mongodb,
      db: dbs.primary,
      dbs,
      daos: loadDaos(ctx, dbs)
    };
  });

  return function init(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

var _default = {
  init
};
exports.default = _default;