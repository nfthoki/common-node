"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.castIds = exports.bind = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _mongodb = require("mongodb");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var findAllCreator = collection => {
  return function findAll() {
    return collection.find(...arguments).toArray();
  };
};

var castIds = (IDS_FIELDS, valueData) => _lodash.default.mapValues(valueData, (value, key) => {
  if (_lodash.default.includes(IDS_FIELDS, key)) {
    if (typeof value === 'string' && _mongodb.ObjectId.isValid(value)) {
      return new _mongodb.ObjectId(value);
    }

    if (Array.isArray(value)) {
      return _lodash.default.map(value, v => typeof v === 'string' && _mongodb.ObjectId.isValid(v) ? new _mongodb.ObjectId(v) : v);
    }

    if (!_lodash.default.isNil(value) && Object.getPrototypeOf(value) === Object.prototype) {
      return _lodash.default.mapValues(value, (nestedValue, nestedKey) => {
        if (_lodash.default.startsWith(nestedKey, '$')) {
          if (typeof nestedValue === 'string' && _mongodb.ObjectId.isValid(nestedValue)) {
            return new _mongodb.ObjectId(nestedValue);
          }

          if (Array.isArray(nestedValue)) {
            return _lodash.default.map(nestedValue, v => typeof v === 'string' && _mongodb.ObjectId.isValid(v) ? new _mongodb.ObjectId(v) : v);
          }
        }

        return nestedValue;
      });
    }
  }

  if (_lodash.default.startsWith(key, '$')) {
    if (typeof value === 'object') {
      if (Array.isArray(value)) {
        return _lodash.default.map(value, nestedValue => {
          if (typeof nestedValue === 'object') {
            return castIds(IDS_FIELDS, nestedValue);
          }

          return nestedValue;
        });
      }

      return castIds(IDS_FIELDS, value);
    }
  }

  return value;
});

exports.castIds = castIds;

var deprecatedFn = fn => () => {
  console.error(fn, 'is deprecated');
};

var bind = (collection, model) => {
  var verbs = ['aggregate', 'bulkWrite', 'countDocuments', 'deleteOne', 'deleteMany', 'distinct', 'estimatedDocumentCount', 'find', 'findOne', 'findOneAndDelete', 'findOneAndReplace', 'findOneAndUpdate', 'initializeOrderedBulkOp', 'initializeUnorderedBulkOp', 'insertOne', 'insertMany', 'replaceOne', 'stats', 'updateOne', 'updateMany', 'mapReduce', 'watch', 'indexes', 'dropIndex', 'indexExists', 'createIndex', 'drop'];

  var mongoLib = _lodash.default.fromPairs(_lodash.default.map(verbs, verb => [verb, collection[verb] ? collection[verb].bind(collection) : deprecatedFn(verb)]));

  mongoLib.of = mongoLib.from = query => castIds(model.castIds || [], query);

  mongoLib.findAll = findAllCreator(collection);
  mongoLib.Collection = collection;
  mongoLib.name = collection.s.namespace.collection;
  mongoLib.db = collection.s.namespace.db;
  return mongoLib;
};

exports.bind = bind;