"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongodb = require("./mongodb");

var _default = {
  createConnection: _mongodb.createConnection,
  init: _mongodb.init,
  toId: _mongodb.toId
};
exports.default = _default;