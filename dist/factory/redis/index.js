"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redis = require("./redis");

var _default = {
  init: _redis.init
};
exports.default = _default;