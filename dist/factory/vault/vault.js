"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.write = exports.remove = exports.read = exports.init = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _path = _interopRequireDefault(require("path"));

var _nodeVault = _interopRequireDefault(require("node-vault"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var init = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, config) {
    var {
      endpoint
    } = config;
    var vault = yield (0, _nodeVault.default)({
      endpoint
    });
    return {
      vault
    };
  });

  return function init(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.init = init;

var login = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (ctx, config, vault) {
    var {
      instances: {
        logger
      }
    } = ctx;
    var {
      roleId,
      secretId
    } = config;
    var {
      client
    } = vault;
    var loginResult = yield client.approleLogin({
      role_id: roleId,
      secret_id: secretId
    });

    var token = _lodash.default.get(loginResult, 'auth.client_token');

    var leaseDuration = _lodash.default.get(loginResult, 'auth.lease_duration');

    if (_lodash.default.isEmpty(token)) {
      logger.error('[vault][login] Could not get client_token', loginResult);
      throw new Error('[vault][login] Could not get client_token');
    }

    return {
      token,
      expires: leaseDuration
    };
  });

  return function login(_x3, _x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}();

var autoRenew = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (ctx, config, vault) {
    var {
      client
    } = vault;

    if (_lodash.default.isEmpty(client.token) || (0, _moment.default)().isAfter(vault.expiredAt)) {
      var requestedAt = (0, _moment.default)();
      var {
        expires
      } = yield login(ctx, config, vault);
      vault.lastLoginAt = requestedAt.valueOf();
      vault.expiredAt = requestedAt.add(expires, 'seconds').valueOf();
    }

    return vault;
  });

  return function autoRenew(_x6, _x7, _x8) {
    return _ref3.apply(this, arguments);
  };
}();

var read = (ctx, config, vault) => {
  var {
    root
  } = config;
  var {
    client
  } = vault;
  return /*#__PURE__*/_asyncToGenerator(function* () {
    yield autoRenew(ctx, config, vault);

    for (var _len = arguments.length, paths = new Array(_len), _key = 0; _key < _len; _key++) {
      paths[_key] = arguments[_key];
    }

    var result = yield client.read("".concat(root, "/data/").concat(_path.default.join(..._lodash.default.map(paths, _lodash.default.toString))));
    return result;
  });
};

exports.read = read;

var write = (ctx, config, vault) => {
  var {
    root
  } = config;
  var {
    client
  } = vault;
  return /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator(function* (value) {
      yield autoRenew(ctx, config, vault);

      for (var _len2 = arguments.length, paths = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        paths[_key2 - 1] = arguments[_key2];
      }

      var result = yield client.write("".concat(root, "/data/").concat(_path.default.join(..._lodash.default.map(paths, _lodash.default.toString))), value);
      return result;
    });

    return function (_x9) {
      return _ref5.apply(this, arguments);
    };
  }();
};

exports.write = write;

var remove = (ctx, config, vault) => {
  var {
    root
  } = config;
  var {
    client
  } = vault;
  return /*#__PURE__*/_asyncToGenerator(function* () {
    yield autoRenew(ctx, config, vault);

    for (var _len3 = arguments.length, paths = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      paths[_key3] = arguments[_key3];
    }

    var result = yield client.delete("".concat(root, "/metadata/").concat(_path.default.join(..._lodash.default.map(paths, _lodash.default.toString))));
    return result;
  });
};

exports.remove = remove;