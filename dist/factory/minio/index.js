"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _minio = require("./minio");

var _default = {
  init: _minio.init
};
exports.default = _default;