"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _googleStorage = require("./google-storage");

var _default = {
  init: _googleStorage.init
};
exports.default = _default;